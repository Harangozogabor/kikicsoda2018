
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ki kicsoda?</title>
    <link rel="icon" type="image/png" href="img/favicon-32x32.png" sizes="32x32" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="wrapper"></div>
<img src="img/logo.png" class="logo">

<nav class="navbar navbar-inverse navbar-default">

    <!-- Navbar brand -->

    <!-- Collapse button -->
    <div class="navbar-header">
        <button class="navbar-toggle collapsed"  type="button"
                data-toggle="collapse" data-target="#id2"
                aria-controls="id2"
                aria-expanded="false" >
            <span class="icon-bar top-bar"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
        </button>
    </div>
    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="id2">

        <!-- Links -->
        <ul class="nav navbar-nav navbar-center navbar-custom">
            <li ><a href="index.html">Ki kicsoda?</a></li>
            <li><a href="ar.html">Áraink</a></li>
            <li><a href="hirdetoknek.php">Hirdetőknek</a></li>
            <li class="active"><a href="megrendeles.php">Adatlap</a></li>
              <!-- <li><a href="partnerek.html">Ők már partnereink</a></li>-->
            <li><a href="kapcsolat.html">Kapcsolat</a></li>
        </ul>
        <!-- Links -->

    </div>
    <!-- Collapsible content -->

</nav>
<div class=" text-center first-container">
    <?php include('form_process.php'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <form id="contact" action="<?= htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="error" ><?= $success ?></span>
                        </div>
                        <div class="col-md-6  col-md-offset-3" >

                            <h4 >Tisztelt partnerünk, a megrendelési ürlapot és az adatlapot a honlapon is kitöltheti, ehhez kérjük töltse ki a következő úrlapot!</h4>

                            <div class="form-group">
                                <label>Teljes név </label>
                                <input required="required" type="text" name="name" class="form-control" value="<?= $name ?>" >
                                <span class="error"><?= $name_error ?></span>
                            </div>
                            <div class="form-group">
                                <label>Email cím </label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                    <input required="required" class="form-control" type="email" name="email" value="<?= $email ?>">
                                    <span class="error"><?= $email_error ?></span>
                            </div>
                            <div class="form-group margin" >
                                <label>Telefonszám: </label>
                                <input required="required" type="tel" name="phone" class="form-control" value="<?= $phone ?>">
                                <span class="error"><?= $phone_error ?></span>
                            </div>
                        </div>
                        <div class="col-md-13 ">
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Számlázás cím: </label>
                                    <input required="required" class="form-control" type="text" name="address1"  value="<?= $address1 ?>">
                                    <span class="error"><?= $address1_error ?></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Szállítási cím: </label>
                                    <input required="required"  class="form-control" type="text" name="address2"  value="<?= $address2?>">
                                    <span class="error"><?= $address2_error ?></span>
                                </div>
                            </div>
                                <div class="col-md-12">
                                    <label>Mely járásban szeretné kérni  a megjelenését? </label>
                                    <select class="form-control" name="jaras" required="required" class="form-control" name="type" id="type" onchange="choose()">
                                        <option>Gyomaendrődi</option>
                                        <option>Szeghalmi</option>
                                        <option>Békési</option>
                                        <option>Sarkadi</option>
                                        <option>Szarvasi</option>
                                        <option>Békéscsabai</option>
                                        <option>Gyulai</option>
                                        <option>Orosházi</option>
                                        <option>Mezőkovácsházi</option>
                                    </select>
                                </div>
                            <div class="col-sm-4 col-sm-offset-4">
                            <div class="form-group">
                                <div class="form-group">
                                    <label>Darabszám: </label>
                                    <input required="required" class="form-control" type="number" name="quantity" id="db" min="1" max="100" value="1" onchange="sum()" >
                                </div>
                            </div>
                        </div>
                            <div class="col-md-6 col-md-offset-3">
                                <label>Összeg (Bruttó ár): </label>
                                <label id="price"  >10500 Ft</label>
                            </div>

                            <div class="form-group">
                                <label class="bigFont">A kiadványban megjelentetni kívánt adataim a következők:</label>

                            </div>
                            <div class="form-group">
                                <label>Leánykori név(ha van)</label>
                                <input  type="text" name="name3" class="form-control" value="<?= $name2 ?>" >

                            </div>
                                <div class="form-group">
                                    <label>Cím: </label>
                                    <input  class="form-control" type="text" name="address2  value="<?= $address2 ?>">
                                </div>

                                <div class="form-group">
                                    <label>Születési hely, idő: </label>
                                    <input class="form-control" type="text" name="szul"  value="<?= $szul ?>">
                                </div>
                            <div class="form-group">
                                <label>Foglalkozás: </label>
                                <input class="form-control" type="text" name="foglalkozas"  value="<?= $foglalkozas ?>">
                            </div>
                            <div class="form-group">
                                <label>Családi állapot: </label>
                                <input class="form-control" type="text" name="csaladi"  value="<?= $csaladi ?>">
                            </div>
                            <div class="form-group">
                                <label>Gyermekek: </label>
                                <textarea class="form-control"  name="gyermekek"  rows="2">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label>Tanulmányok: </label>
                                <textarea class="form-control"  name="tanulmanyok"  rows="5">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label>Kitüntetés(ek), elismerés(ek), díj(ak): </label>
                                <textarea class="form-control"  name="elismeresek"  rows="5">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label>Hobbi(k): </label>
                                <textarea class="form-control"  name="hobbik"  rows="5">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label>Munkásság: </label>
                                <textarea class="form-control"  name="munkassag"  rows="10">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label>Egyéb: </label>
                                <textarea class="form-control"  name="egyeb"  rows="5">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="adatvédelem">
                                    Elolvastam és elfogadom az <a href="adat.pdf">adatvédelmi nyilatkozatot<a/></label>
                                <input type="checkbox"  onchange="document.getElementById('send').disabled = !this.checked;" />
                            </div>
                        <div class="col-md-13">
                            <button type="submit" id="send" disabled class="btn btn-primary pull-right myButton"  name="submit" value="Küldés" data-submit="...Sending">Küldés</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
    <div class="row">
        <div class="col-md-4 end">
            <h4 class="adat" id="up" >
                Szervez-Oktat KFT 5451 <br>Öcsöd, Deák F. út 27/c<br>
                Postacím:  5600 Békéscsaba Kazinczy utca 4 B/213 iroda<br>
                e-mail: kikicsoda2017@gmail.com<br>
            </h4>
        </div>
        <div class="col-md-4 end" >
            <img src="img/logo.png" class="logo1" >
        </div>
        <div class="col-md-4 end">
            <h4 class="adat" >
    Terület-felelős: Irimiás István<br>
                Telefon: 06-70/335-29-04<br>
                Ügyvezető és Kiadó: Lovász Sándor<br>
                Telefon: 06-30/945-19-87<br>
            </h4>
        </div>
    </div>
</div>



</body>
<script>
    function sum() {
        var value= document.getElementById('db').value;
        document.getElementById('price').innerHTML=value*10500+' Ft'
    }
</script>
</html>