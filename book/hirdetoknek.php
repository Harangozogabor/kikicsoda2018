
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ki kicsoda?</title>
    <meta charset="utf-8">
    <link rel="icon" type="image/png" href="img/favicon-32x32.png" sizes="32x32" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="wrapper"></div>
<img src="img/logo.png" class="logo">

<nav class="navbar navbar-inverse navbar-default">

    <!-- Navbar brand -->

    <!-- Collapse button -->
    <div class="navbar-header">
        <button class="navbar-toggle collapsed" type="button"
                data-toggle="collapse" data-target="#id2"
                aria-controls="id2"
                aria-expanded="false" >
            <span class="icon-bar top-bar"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
        </button>
    </div>
    <!-- Collapsible content -->
    <div class="collapse navbar-collapse" id="id2">

        <!-- Links -->
        <ul class="nav navbar-nav navbar-center navbar-custom">
            <li ><a href="index.html">Ki kicsoda?</a></li>
            <li><a href="ar.html">Áraink</a></li>
            <li class="active"><a href="hirdetoknek.php">Hirdetőknek</a></li>
            <li ><a href="megrendeles.php">Adatlap</a></li>
           <!-- <li><a href="partnerek.html">Ők már partnereink</a></li>-->
            <li><a href="kapcsolat.html">Kapcsolat</a></li>
        </ul>
        <!-- Links -->

    </div>
    <!-- Collapsible content -->

</nav>

<div class="text-center first-container">
    <?php include('form_process2.php'); ?>
    <div class="form-style-5" >
        <form id="contact"  action="<?= htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
            <fieldset>

                <div class="row">
                    <div class="col-md-6  col-md-offset-3" >
                        <h4 >Megrendelem a Szervez-Oktat
                            KFT gondozásában megjelenő Ki kicsoda Békés megyében 2018? című könyvben egy hirdetést:</h4>
                        <div class="form-group">
                            <label>Teljes név </label>
                            <input required="required" type="text" name="name" class="form-control" value="<?= $name ?>" >
                            <span class="error"><?= $name_error ?></span>
                        </div>
                        <div class="form-group">
                            <label>Email cím </label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                <input required="required" class="form-control" type="email" name="email" value="<?= $email ?>">
                                <span class="error"><?= $email_error ?></span>
                            </div>
                         </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <label>Számlázás cím: </label>
                        <input  required="required" class="form-control" type="text" name="address1"  value="<?= $address1 ?>">
                        <span class="error"><?= $address1_error ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <label>Ügyintéző neve: </label>
                        <input required="required" class="form-control" type="text" name="name2"  value="<?= $name2?>">
                        <span class="error"><?= $name2_error ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <label>Ügyintéző telefonszáma: </label>
                        <input type="tel" class="form-control" name="phone2"  value="<?= $phone2?>">
                        <span class="error"><?= $phone2_error ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <label>Ügyintéző email címe: </label>
                        <input required="required" class="form-control" type="text" name="email2"  value="<?= $email2?>">
                        <span class="error"><?= $email2_error ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <label>Hirdetés típus: </label>
                        <select required="required" class="form-control" name="type" id="type" onchange="choose()">
                            <option>Belső oldal, teljes</option>
                            <option>Belső oldal, ½ oldal</option>
                            <option>Belső oldal, ¼ oldal</option>
                            <option>Címoldal alsó 1/5 része</option>
                            <option>Hátoldal egész</option>
                            <option>Hátoldal ½ oldal</option>
                            <option>Borító 1 teljes oldal</option>
                            <option>Borító 1 , ½ oldal</option>
                            <option>Borító 3, teljes oldal</option>
                            <option>Borító 3, ½ oldal</option>
                            <option>Főszponzor</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label for="adatvédelem">
                        Elolvastam és elfogadom az <a href="adat.pdf">adatvédelmi nyilatkozatot<a/></label>
                    <input type="checkbox"  onchange="document.getElementById('send').disabled = !this.checked;" />
                </div>
                <fieldset>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <label>Összeg: </label>
                            <label id="price"  >150000 Ft + Áfa</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <span class="error"><?= $success ?></span>
                        </div>
                    </div>
                </fieldset>
            </fieldset>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <button type="submit" id="send" disabled class="btn btn-primary pull-right myButton" name="submit" value="Küldés" data-submit="...Sending">Küldés</button>
                </div>
            </div>

        </form>
    </div>
</div>
<div class="row" >
    <div class="col-md-4 end">
        <h4 class="adat" id="up" >
            Szervez-Oktat KFT 5451 <br>Öcsöd, Deák F. út 27/c<br>
            Postacím:  5600 Békéscsaba Kazinczy utca 4 B/213 iroda<br>
            e-mail: kikicsoda2017@gmail.com<br>
        </h4>
    </div>
    <div class="col-md-4 end" >
        <img src="img/logo.png" class="logo1" >
    </div>
    <div class="col-md-4 end">
        <h4 class="adat">
            Terület-felelős: Irimiás István<br>
            Telefon: 06-70/335-29-04<br>
            Ügyvezető és Kiadó: Lovász Sándor<br>
            Telefon: 06-30/945-19-87<br>
        </h4>
    </div>
</div>
</div>



</body>
<script>
    function sum() {
        var value= document.getElementById('db').value;
        document.getElementById('price').innerHTML=value*10500+' Ft'
    }
    function choose() {
        var value= document.getElementById('type').value;
        if(value==='Belső oldal, teljes'){
            document.getElementById('price').innerHTML=150000+' Ft + Áfa'
        }else         if(value==='Belső oldal, ½ oldal'){
            document.getElementById('price').innerHTML=100000+' Ft + Áfa'
        }else         if(value==='Belső oldal, ¼ oldal'){
            document.getElementById('price').innerHTML=65000+' Ft + Áfa'
        }else         if(value==='Címoldal alsó 1/5 része'){
            document.getElementById('price').innerHTML=500000+' Ft + Áfa'
        }else         if(value==='Hátoldal egész'){
            document.getElementById('price').innerHTML=300000+' Ft + Áfa'
        }else         if(value==='Hátoldal ½ oldal'){
            document.getElementById('price').innerHTML=200000+' Ft + Áfa'
        }else         if(value==='Borító 1 teljes oldal'){
            document.getElementById('price').innerHTML=200000+' Ft + Áfa'
        }else         if(value==='Borító 1 , ½ oldal'){
            document.getElementById('price').innerHTML=150000+' Ft + Áfa'
        }else         if(value==='Borító 3, teljes oldal'){
            document.getElementById('price').innerHTML=200000+' Ft + Áfa'
        }else         if(value==='Borító 3, ½ oldal'){
            document.getElementById('price').innerHTML=150000+' Ft + Áfa'
        }else if(value==='Főszponzor'){
            document.getElementById('price').innerHTML=5000000+' Ft + Áfa'
        }

    }
</script>
</html>