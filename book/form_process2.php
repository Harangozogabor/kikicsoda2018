<?php

// define variables and set to empty values
$name_error = $name2_error = $email_error = $email2_error = $phone_error= $phone2_error = $address1_error = $name2_error = $quantity_error = "";
$name = $email= $email2 = $phone =$name2=$phone2 =$address1 = $name2 = $quantity = $success = $full_price = $message =$what= "";

//form is submitted with POST method
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
        $name_error = "Adja meg a nevét!";
    } else {
        $name = test_input($_POST["name"]);

    }
    if (empty($_POST["email"])) {
        $email_error = "Adja meg a email címét!";
    } else {
        $email = test_input($_POST["email"]);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email_error = "Nem érvényes email cím formátum!";
        }
    }


    if (empty($_POST["address1"])) {
        $address1_error = "Adja meg a számlázási címét!";
    } else {
        $address1 = test_input($_POST["address1"]);
        // check if URL address syntax is valid (this regular expression also allows dashes in the URL)

    }
    if (empty($_POST["name2"])) {
        $name2_error = "Adja meg az ügyintéző nevét!";
    } else {
        $name2 = test_input($_POST["name2"]);

    }

    if (empty($_POST["phone2"])) {
        $phone2_error = "Adja meg a telefonszámát!";
    } else {
        $phone2 = test_input($_POST["phone2"]);

    }
    if (empty($_POST["email2"])) {
        $email2_error = "Adja meg a email címét!";
    } else {
        $email2 = test_input($_POST["email2"]);
        if (!filter_var($email2, FILTER_VALIDATE_EMAIL)) {
            $email2_error = "Nem érvényes email cím formátum!";
        }
    }

    $quantity = test_input($_POST["type"]);

    if ($name_error == '' and $email_error == '' and $phone_error == '' and $email2_error == '' and  $phone2_error == '' and $name2_error == '' and $address1_error == '') {
        $message = '';
        unset($_POST['submit']);
        $message="A megrendelő neve: " . $name . PHP_EOL .
            "A megrendelő email címe: " . $email .PHP_EOL .
            "A megrendelő számlázási címe: " . $address1 .PHP_EOL .
            "Az ügyintéző neve: " . $name2 .PHP_EOL .
            "Az ügyintéző telefonszáma: " . $phone2 .PHP_EOL .
            "Az ügyintéző email címe: " . $email2 .PHP_EOL .
            "Hirdetés típusa: " . $quantity ;

        $what="Új hirdetést szeretnének közzé tenni, megrendelő: ". $name;
        $to = 'kikicsoda2017@gmail.com';
        if (mail($to, $what, $message)) {
            $success = "A hirdetési igényét elküldte, ügyintézőnk hamarosan felveszi önnel a kapcsolatot!";
            $phone2=$email2=$name2=$name = $email = $phone = $address1 = $address2 = $quantity  = $full_price = $message =$what= "";
        }
    }

}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}