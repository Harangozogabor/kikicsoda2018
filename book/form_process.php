<?php

// define variables and set to empty values
$name_error = $email_error = $phone_error = $address1_error = $address2_error = $quantity_error = "";
$name =$name2=$address2=$szul=$foglalkozas=$csaladi=$gyermekek=$tanulányok=$elismerések=$hobbik=$munkassag=$egyeb= $email = $phone = $address1 = $address2 = $quantity = $success = $full_price = $message =$what= "";

//form is submitted with POST method
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
        $name_error = "Adja meg a nevét!";
    } else {
        $name = test_input($_POST["name"]);

    }
    if (empty($_POST["email"])) {
        $email_error = "Adja meg a email címét!";
    } else {
        $email = test_input($_POST["email"]);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email_error = "Nem érvényes email cím formátum!";
        }
    }

    if (empty($_POST["phone"])) {
        $phone_error = "Adja meg a telefonszámát!";
    } else {
        $phone = test_input($_POST["phone"]);

    }

    if (empty($_POST["address1"])) {
        $address1_error = "Adja meg a számlázási címét!";
    } else {
        $address1 = test_input($_POST["address1"]);
        // check if URL address syntax is valid (this regular expression also allows dashes in the URL)

    }
    if (empty($_POST["address2"])) {
        $address2_error = "Adja meg a szállítási címét!";
    } else {
        $address2 = test_input($_POST["address2"]);
        // check if URL address syntax is valid (this regular expression also allows dashes in the URL)

    }

    $quantity = test_input($_POST["quantity"]);
    $name2 = test_input($_POST["name3"]);
    $szul = test_input($_POST["szul"]);
    $foglalkozas = test_input($_POST["foglalkozas"]);
    $csaladi = test_input($_POST["csaladi"]);
    $gyermekek = test_input($_POST["gyermekek"]);
    $tanulmanyok = test_input($_POST["tanulmanyok"]);
    $elismeresek = test_input($_POST["elismeresek"]);
    $hobbik = test_input($_POST["hobbik"]);
    $munkassag = test_input($_POST["munkassag"]);
    $egyeb = test_input($_POST["egyeb"]);

    if ($name_error == '' and $email_error == '' and $phone_error == '' and $address2_error == '' and $address1_error == '') {
        $message = '';
        unset($_POST['submit']);
        $message="A megrendelő neve: " . $name . PHP_EOL .
            "A megrendelő telefonszáma: " . $phone .PHP_EOL .
            "A megrendelő email címe: " . $email .PHP_EOL .
            "A megrendelő számlázási címe: " . $address1 .PHP_EOL .
            "A megrendelő szállítási címe: " . $address2 .PHP_EOL .
            "Kivánt mennyiség: " . $quantity . "db melynek értéke: " .($quantity*10500) ."Ft" .PHP_EOL .
            "A megrendelő leánykori neve: " . $name2 .PHP_EOL .
            "A megrendelő születési ideje és helye: " . $szul .PHP_EOL .
            "A megrendelő családi állapota: " . $csaladi .PHP_EOL .
            "A megrendelő foglalkozása: " . $foglalkozas .PHP_EOL .
            "A megrendelő gyermekei: " . $gyermekek .PHP_EOL .
            "A megrendelő tanulmányai: " . $tanulmanyok .PHP_EOL .
            "A megrendelő Kitüntetései, elismerései, díjai: " . $elismeresek .PHP_EOL .
            "A megrendelő hobbiai: " . $hobbik .PHP_EOL .
            "A megrendelő munkássága: " . $munkassag .PHP_EOL .
            "Egyéb: " . $egyeb .PHP_EOL .




            $what="Új megrendelés érkezett, megrendelő: ". $name;
        $to = 'kikicsoda2017@gmail.com';
        if (mail($to, $what, $message)) {
            $success = "A rendelését elküldte, ügyintézőnk hamarosan felveszi önnel a kapcsolatot!";
            $name=$name2=$szul=$foglalkozas=$csaladi= $email = $phone = $address1 = $address2 = $quantity  = $full_price = $message =$what= "";
        }
    }

}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}